use chrono::{TimeZone, Utc};
use colored::*;
use std::{collections::BinaryHeap, env, fs, time::UNIX_EPOCH};

#[derive(Ord, Eq)]
struct Note {
    time: u64,
    text: String,
}
impl PartialOrd for Note {
    fn lt(&self, other: &Self) -> bool {
        self.time.lt(&other.time)
    }
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        self.time.partial_cmp(&other.time)
    }
}
impl PartialEq for Note {
    fn eq(&self, other: &Self) -> bool {
        self.time.eq(&other.time)
    }
}

fn main() {
    //Bonus:
    //Add a single level of fuzzy finding

    let args: Vec<String> = env::args().collect();
    if args.len() < 2 {
        println!("Expected a search word!");
        return;
    }
    let mut search_word = args[1].clone();
    let mut save_to_file = false;
    let mut reverse_sort = false;
    if search_word.starts_with("-") {
        if search_word.contains("r") {
            reverse_sort = true;
        }
        if search_word.contains("s") {
            save_to_file = true;
        }
        search_word = args[2].clone();
    }

    let mut file_title = search_word.clone();
    let mut queue: BinaryHeap<Note> = BinaryHeap::new();

    for entry in
        fs::read_dir(std::env::current_dir().expect("Environment current directory failed"))
            .expect("Read currend directory failed")
    {
        if let Ok(dir_ent) = entry {
            if let Ok(ft) = dir_ent.file_type() {
                if !ft.is_file() {
                    continue;
                }
            } else {
                continue;
            }
            if let Ok(file) = fs::read_to_string(dir_ent.path()) {
                //Here we are in a file
                let mut buff: Vec<String> = Vec::new();
                let mut found_section = false;
                let mut print_header = true;
                let file_seconds = dir_ent
                    .metadata()
                    .expect("Failed to get file metadata")
                    .created()
                    .expect("Failed to get created time out of metadata")
                    .duration_since(UNIX_EPOCH)
                    .expect("Duration since epoch failed")
                    .as_secs();
                let file_date = Utc //Formated 5-22-23
                    .timestamp_opt(
                        file_seconds
                            .try_into()
                            .expect("Failed to convert duration to seconds"),
                        0,
                    )
                    .unwrap()
                    .format("%-m-%-d-%y");

                let lines = file.split_terminator("\n").into_iter();
                for line in lines {
                    if line.starts_with("### ") {
                        found_section = line.to_lowercase().contains(&search_word.to_lowercase());
                        if found_section && print_header {
                            print_header = false;
                            let property_name = &line[4..];
                            file_title = property_name.clone().to_owned();
                            let text = format!("{} {}\n", property_name.red(), file_date);
                            buff.push(text);
                        }
                    } else {
                        if line.contains("---") {
                            found_section = false;
                        }
                        if found_section {
                            buff.push(format!("{}\n", line));
                        }
                    }
                }
                queue.push(Note {
                    time: file_seconds,
                    text: buff.join(""),
                });
                buff.clear();
            }
        }
    }
    let mut sorted_out = queue.into_sorted_vec();
    if reverse_sort {
        sorted_out.reverse()
    }
    let sorted_out = sorted_out
        .iter()
        .map(|note| note.text.trim_end().to_owned())
        .collect::<Vec<String>>()
        .join("\n")
        .trim()
        .to_owned()
        + "\n";
    if save_to_file {
        let path = "property-notes/".to_owned() + &file_title + ".md";
        fs::write(path.clone(), sorted_out).expect("Error writing to file");
        println!("Saved to {}", path);
    } else {
        print!("{}", sorted_out);
    }
}
